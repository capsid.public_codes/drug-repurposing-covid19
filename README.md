This repository provides source codes of our drug repurposing study for COVID-19 which titled as "Molecular-evaluated and explainable drug repurposing for COVID-19 using ensemble knowledge graph embedding".
Here we describe the step by step procedure for using the source codes.

**Step-1: Learning the DRKG knowledge graph embeddings**

For learning DRKG embeddings, necessary files are put in the ./KG-Embedding-model/ directory. Please follow the following steps to learn embeddings.

      Step-1.1: Preparing datasets
      For splitting the cleaned DRKG knowledge graph (KG) dataset, apply 90/5/5 split rule to prepare training/valid/test sets. To minimize any biasness in downstream link prediction results, apply the split rule to triples for each relation. For experiments, we provide the following dataset files in KG-Embedding-model/KG_Data/DRKG_v3/ directory of this repository.
            entity2id.txt: The list of DRKG entities and their id mapping 
            relation2id.txt: The list of DRKG relations and their id mapping
            train2id.txt: The training triple set. Each line gives one triple in (head id, tail id, relation id) format.
            test2id.txt: The test triple set. Each line gives one triple in (head id, tail id, relation id) format.
            valid2id.txt: The valid triple set. Each line gives one triple in (head id, tail id, relation id) format.

      Step-1.2: Training KG embedding models (TransE, TransH, and DistMult)
            - For training KG embedding models, set the parameters in the train.py file
                  -- task_dir: the directory to dataset, default='./KG_Data/DRKG_v3'
                  --model: the embedding model from [TransE, TransH, DistMult], default='TransE'
                  --margin: the margin between positive and negative triple scores for pairwise training, default=4.0
                  --hidden_dim: the embedding size, default=100
                  --p: the norm for scoring function from [1,2], default=1
                  --lr: the learning rate for the Adam optimizer, default=0.001
                  --n_epoch: the number of training epochs, default=10000
                  --n_batch: the training batch size, default=4096
                  --N1: the size of candidate negative set for SNS negative sampling, default=100
                  --N2: the top-N2 for the SNS, default=1
                  --test_batch_size: the test batch size, default=30
            - Run the train.py file for each of the [TransE, TransH, DistMult] models

            After finising the experiments, you will get the embeddings of entities and relations in the above task_dir. The embeddings will be used in the downstream drug prodiction task.

**Step-2: Predicting potential drugs for COVID-19**

For predicting potential drugs for COVID-19, necessary files are put in the ./Prediction-model/ directory. Please follow the following steps for predicting drugs for COVID-19.

      Step-2.1: Preparing datasets
      The training (valid, test) triples with 'Treat' relation for the embedding models are considered as training (valid, test) examples for the drug prediction model. 
      Step-2.2: Training of prediction model
      Run the following scripts to perform different experiments on the prediction model.
            - MLP_V.py : Run this file to perform 10-fold cross validation. Set embedding files directories, training and test example files directories for each training-test sets. The training and test sets for each fold are available in ./Prediction-model/10-fold/ directory.
            - MLP_Perm.py : Run this file to perform permutation test. Set embedding files directories, training and test example files directories and number of permutations (default is 100) for each training-test sets. Total 1000 permutation test files are available in the ./Prediction-model/Perm1/ directory.
            - MLP.py: Run this file to rank potential (drug, disease-target) pairs. All (drug, disease-target) pairs are available in the ./Candidate_pair.txt file where first column is for drug id and second column is for disease-target id (this id comes from entity2id.txt file). Please check the following output files.
                  -- Ranked_drugs.csv: The ranked predicted (drug- disease-target) pairs.
                  -- Top100_drugs.csv: The list of top-100 predicted drugs for COVID-19 disease targets. (You can set top-N to desired number of drugs for further processing)

**Step-3: Generating explanations for predictions**

For generating explanations for top-100 predictions, necessary files are put in the ./Explanation/ directory. Run the Explain_path_generation.ipynb fileto generate explanations of top-100 (Drug, Disease target) pairs by setting these parameters: 
      -- pr_triple_file: the top-100 predictions file, default='Top100_drugs.csv'
      -- rule_file: the rule set, default='Rule-set.xlsx'

For any difficulties, please write me: kamrul.islam@loria.fr
