import numpy as np
_,ar,_,_,_=np.random.get_state()
seed=1528155142#ar[1] #1669768389 #3519291485 #2147483648 #22506088 #2413843216 #2311804307 #1200295143 #2032192475 Best: 10(1528155142, 3978893405) 2147483648
np.random.seed(seed)

import tensorflow as tf
tf.random.set_seed(seed)

import torch.nn.functional as fn
import torch
# from keras.callbacks import Callback
from keras.models import Sequential
from keras.layers import Dense, LSTM
from keras.utils import np_utils
from keras.layers.core import Dense, Activation, Dropout
from keras.callbacks import EarlyStopping
from tensorflow import keras
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from convers_pca import convers_pca

#Standardize data PCA analysis
std = StandardScaler()
pca = convers_pca()

load=False
save=False

#load the entity list
E = {}
E1 = {} 
with open('entity2id.txt','r') as flr:
    line=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (name, idd)=line.strip().split('\t')
        E[int(idd)]=name
        E1[name]=int(idd)
        line=flr.readline()


#load trial drugs
ITD = {}
trial_ids = []
with open('trial_drugs.tsv','r') as flr:
    line=flr.readline()
    while(len(line)>0):
        (rnk,nm,idd)=line.strip().replace(' ','-').split()
        ITD[idd]=nm
        ids='Compound::'+idd
        if(ids in list(E1.keys())):
            trial_ids.append(E1[ids])
        
        line=flr.readline()

#import entity embedding
#Get the embedding
NE=len(E)
Dim=100
#load entity embeddings
EM = np.zeros((NE,Dim))
print('Loading embedding...')
with open('TransE_Em_Ent.txt','r') as flr:
    for idd in range(0,NE):
        vec = []
        totalstring=flr.readline()
        while(']' not in totalstring):
            line=flr.readline()
            totalstring=totalstring+' '+line.strip()           
        
        totalstring=totalstring.replace('[','').replace(']','').strip()
        arr=totalstring.split(' ')
        for e in arr:
            if(len(e)>0):
                vec.append(float(e))
                
        if(len(vec)==Dim):
            for i in range(Dim):
                EM[idd,i]=vec[i]
        else:
            print(idd)
            print(len(vec))
            break

#load entity embeddings
EM2 = np.zeros((NE,Dim))
print('Loading embedding...')
with open('DistMult_Em_Ent.txt','r') as flr:
    for idd in range(0,NE):
        vec = []
        totalstring=flr.readline()
        while(']' not in totalstring):
            line=flr.readline()
            totalstring=totalstring+' '+line.strip()           
        
        totalstring=totalstring.replace('[','').replace(']','').strip()
        arr=totalstring.split(' ')
        for e in arr:
            if(len(e)>0):
                vec.append(float(e))
                
        if(len(vec)==Dim):
            for i in range(Dim):
                EM2[idd,i]=vec[i]
        else:
            print(idd)
            print(len(vec))
            break

#load entity embeddings
EM4 = np.zeros((NE,Dim))
print('Loading embedding...')
with open('TransH_Em_Ent.txt','r') as flr:
    for idd in range(0,NE):
        vec = []
        totalstring=flr.readline()
        while(']' not in totalstring):
            line=flr.readline()
            totalstring=totalstring+' '+line.strip()           
        
        totalstring=totalstring.replace('[','').replace(']','').strip()
        arr=totalstring.split(' ')
        for e in arr:
            if(len(e)>0):
                vec.append(float(e))
                
        if(len(vec)==Dim):
            for i in range(Dim):
                EM4[idd,i]=vec[i]
        else:
            print(idd)
            print(len(vec))
            break

#PCA analysis
EM_tmp = std.fit_transform(EM)
pca.fit(EM_tmp)
V=pca.explained_variance_unsorted
idxs = np.transpose(np.argwhere(V>=1.0))[0]
EM1=np.empty((EM_tmp.shape[0], idxs.shape[0]))
for i in range(idxs.shape[0]):
    ind=idxs[i]
    EM1[:,i]=EM[:,ind]

EM_tmp = std.fit_transform(EM2)
pca.fit(EM_tmp)
V=pca.explained_variance_unsorted
idxs = np.transpose(np.argwhere(V>=1.0))[0]
EM3=np.empty((EM_tmp.shape[0], idxs.shape[0]))
for i in range(idxs.shape[0]):
    ind=idxs[i]
    EM3[:,i]=EM2[:,ind]

EM_tmp = std.fit_transform(EM4)
pca.fit(EM_tmp)
V=pca.explained_variance_unsorted
idxs = np.transpose(np.argwhere(V>=1.0))[0]
EM5=np.empty((EM_tmp.shape[0], idxs.shape[0]))
for i in range(idxs.shape[0]):
    ind=idxs[i]
    EM5[:,i]=EM4[:,ind]


#EM1=std.fit_transform(EM)
#EM3=std.fit_transform(EM2)
#EM5=std.fit_transform(EM4)

#EM1=EM
#EM3=EM2
#EM5=EM4

#new size
esize1=EM1.shape[1] 
esize3=EM3.shape[1] 
esize5=EM5.shape[1] 


part1=esize1
part2=part1+esize3
part3=part2+esize5
part4=part3+esize1
part5=part4+esize3
part6=part5+esize5

print('Preparing dataset.....')
TS= 261080 #1096536#261080 #156648 #261080 #365512 #574376
X = np.empty((TS,part6))
Y = np.empty(TS)

row=-1
#Prepare the positive train embedding (h,r,t) sequence
with open('./10-fold/MLP_Train_pos_f1.txt','r') as flr:
    line=flr.readline()
    while(len(line)>0):
        row=row+1
        (h,t)=line.strip().split('\t')
        (h,t)=(int(h),int(t))
               
        X[row,0:part1]=EM1[h,:]
        X[row,part1:part2]=EM3[h,:]
        X[row,part2:part3]=EM5[h,:]

        X[row,part3:part4]=EM1[t,:]
        X[row,part4:part5]=EM3[t,:]
        X[row,part5:part6]=EM5[t,:]

        Y[row]=1.0
        
        line=flr.readline()

#Prepare the negative train embedding (h,r,t) sequence
with open('./10-fold/MLP_Train_neg_f1.txt','r') as flr: #MLPneg2 #MLPneg10
    line=flr.readline()
    while(len(line)>0):
        row=row+1
        (h,t)=line.strip().split('\t')
        (h,t)=(int(h),int(t))

        X[row,0:part1]=EM1[h,:]
        X[row,part1:part2]=EM3[h,:]
        X[row,part2:part3]=EM5[h,:]

        X[row,part3:part4]=EM1[t,:]
        X[row,part4:part5]=EM3[t,:]
        X[row,part5:part6]=EM5[t,:]
        Y[row]=0.0
        
        line=flr.readline()

#Prepare test data
DH = []
DR = []
DT = []
Labels = []

with open('./10-fold/MLP_Test_pos_f1.txt','r') as flr:
    line=flr.readline()
    while(len(line)>0):
        (h,t)=line.strip().split('\t')
        (h,t)=(int(h),int(t))
    
        DH.append(h)
        DT.append(t)
        Labels.append(1.0)
        
        line=flr.readline()

with open('./10-fold/MLP_Test_neg_f1.txt','r') as flr:
    line=flr.readline()
    while(len(line)>0):
        (h,t)=line.strip().split('\t')
        (h,t)=(int(h),int(t))
    
        DH.append(h)
        DT.append(t)
        Labels.append(0.0)
        
        line=flr.readline()

Data=np.empty((len(DH),part6))
for i in range(len(DH)):
    h=DH[i]
    t=DT[i]
       
    Data[i,0:part1]=EM1[h,:]
    Data[i,part1:part2]=EM3[h,:]
    Data[i,part2:part3]=EM5[h,:]

    Data[i,part3:part4]=EM1[t,:]
    Data[i,part4:part5]=EM3[t,:]
    Data[i,part5:part6]=EM5[t,:]

Labels = np.array(Labels)

#for training model

input_dim = X.shape[1]
#X = std.fit_transform(X)

N_EPOCHS=2000
train_batch=2048
dropout=0.1
callback = EarlyStopping(monitor='loss', patience=30, restore_best_weights=True)  #30=10
print('Preparing prediction model....')

model = Sequential()

#Input layer
nn=input_dim
model.add(Dense(nn, input_dim=input_dim,use_bias=True))
model.add(Activation('relu'))
model.add(Dropout(dropout))

#Hidden layer-1
nn=int(nn*0.5)
model.add(Dense(nn,use_bias=True))
model.add(Activation('relu'))
model.add(Dropout(dropout))

#Hidden layer-2
nn=int(nn*0.5)
model.add(Dense(nn,use_bias=True))
model.add(Activation('relu'))
model.add(Dropout(dropout))

#output layer
model.add(Dense(1, activation='sigmoid')) 
model.compile(loss='mean_squared_error', optimizer='Adam')#,metrics=[tf.keras.metrics.MeanSquaredError()]) #mean_squared_error #binary_crossentropy

#model = keras.models.load_model('path/to/location')
if(load==True):
    model = keras.models.load_model('Model')

print(model.summary())
model.fit(X, Y, epochs=N_EPOCHS, batch_size=train_batch, verbose=1, shuffle=True, callbacks=[callback])#, validation_data=(Data1, Labels1))

if(save==True):
    model.save('MLPModel')

v1 = model.predict(Data)
v1=v1.T[0]

#predict the plausabulity
result = model.evaluate(Data, Labels, verbose = 0)
print(result)
flw = open('./10-fold/MLP_Result_f1.csv','a')
flw.writelines('Compound\tDisease\tOrginial\tPrediction')
for i in range(len(DH)):
    h=DH[i]
    t=DT[i]
    org=Labels[i]
    pred=v1[i]
    line = '\n'+str(h)+'\t'+str(t)+'\t'+str(org)+'\t'+str(pred)
    flw.writelines(line)

flw.close()

