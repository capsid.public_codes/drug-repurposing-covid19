import numpy as np

class convers_pca():
    def __init__(self, no_of_components=None):
        self.no_of_components = no_of_components
        self.eigen_values = None
        self.eigen_vectors = None
        
    def transform(self, x):
        return np.dot(x - self.mean, self.projection_matrix.T)
    
    def inverse_transform(self, x):
        return np.dot(x, self.projection_matrix) + self.mean
    
    def fit(self, x):
        self.no_of_components = x.shape[1] if self.no_of_components is None else self.no_of_components
        #print('Computing mean')
        self.mean = np.mean(x, axis=0)
        #print('Computing covarient metrix')
        cov_matrix = np.cov(x - self.mean, rowvar=False)
        #print('Computing eign values')
        self.eigen_values, self.eigen_vectors = np.linalg.eig(cov_matrix)
        self.eigen_vectors = self.eigen_vectors.T
        #print('Sorting components')
        self.sorted_components = np.argsort(self.eigen_values)[::-1]
        
        self.projection_matrix = self.eigen_vectors[self.sorted_components[:self.no_of_components]]
        self.explained_variance = self.eigen_values[self.sorted_components]
        #print(self.explained_variance)
        self.explained_variance_unsorted = self.eigen_values
        self.explained_variance_ratio = self.explained_variance / self.eigen_values.sum()
        #print(self.explained_variance_ratio)
