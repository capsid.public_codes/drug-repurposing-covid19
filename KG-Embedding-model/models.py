import torch
import torch.nn.functional as F
import torch.nn as nn
import os

class BaseModule(nn.Module):
    def __init__(self, n_ent, n_rel, args):
        super(BaseModule, self).__init__()
        self.lamb = args.lamb
        self.p = args.p         
        self.margin = args.margin
        self.temp = args.temp

    def init_weight(self):
        for param in self.parameters():
            nn.init.xavier_uniform_(param.data)

    def score(self, head, tail, rela):
        raise NotImplementedError

    def dist(self, head, tail, rela):
        raise NotImplementedError

    def prob_logit(self, head, tail, rela, flag):
        raise NotImplementedError

    def prob(self, head, tail, rela, flag):
        return F.softmax(self.prob_logit(head, tail, rela, flag), dim=-1)

    def pair_loss(self, head, tail, rela, n_head, n_tail):
        d_pos = self.dist(head, tail, rela)
        d_neg = self.dist(n_head, n_tail, rela)
        return torch.sum(F.relu(self.margin + d_pos - d_neg))

    def point_loss(self, head, tail, rela, label):
        softplus = torch.nn.Softplus().cuda()
        score = self.forward(head, tail, rela)
        score = torch.sum(softplus(-1*label*score))
        return score

class TransEModule(BaseModule):
    def __init__(self, n_ent, n_rel, args):
        super(TransEModule, self).__init__(n_ent, n_rel, args)
        self.rel_embed = nn.Embedding(n_rel, args.hidden_dim)
        self.ent_embed = nn.Embedding(n_ent, args.hidden_dim)
        self.init_weight()

    def forward(self, head, tail, rela):
        shape = head.size()
        head = head.contiguous().view(-1)
        tail = tail.contiguous().view(-1)
        rela = rela.contiguous().view(-1)
        head_embed = F.normalize(self.ent_embed(head),2,-1)
        tail_embed = F.normalize(self.ent_embed(tail),2,-1)
        rela_embed = self.rel_embed(rela)
        return torch.norm(tail_embed - head_embed - rela_embed, p=self.p, dim=-1).view(shape)

    def dist(self, head, tail, rela):
        return self.forward(head, tail, rela)

    def score(self, head, tail, rela):
        return self.forward(head, tail, rela)

    def get_emb(self, h, r):
        h = torch.from_numpy(h).type(torch.LongTensor)
        h = h.contiguous().view(-1)
        h_embed = self.ent_embed(h)

    def candidate(self, ent1, ent2, rela, flag):
        shape = ent1.size()
        ent1 = ent1.contiguous().view(-1)
        ent2 = ent2.contiguous().view(-1)
        rela = rela.contiguous().view(-1)
        ent1_embed = F.normalize(self.ent_embed(ent1), 2, -1)
        ent2_embed = F.normalize(self.ent_embed(ent2), 2, -1)
        vec=torch.norm(ent1_embed - ent2_embed, p=self.p, dim=-1).view(shape)
        vec=torch.abs(vec)
        return vec

    def prob_logit(self, head, tail, rela, flag):
        return -self.candidate(head, tail, rela, flag) / self.temp

    def writeemb(self, N, R, task_dir, mdl):
        fll=os.path.join(task_dir, mdl +'_Em_Ent.txt')
        if(os.path.exists(fll)):
            os.remove(fll)
        writer=open(fll,'a')

        for e in range(0,N):
        	e=torch.tensor([e]).cuda()
        	e_embed = F.normalize(self.ent_embed(e), 2, -1)
        	writer.writelines(str(torch.flatten(e_embed).detach().cpu().numpy()))
        	writer.writelines('\n')

        writer.close()

        fll=os.path.join(task_dir, mdl +'_Em_Rel.txt')
        if(os.path.exists(fll)):
            os.remove(fll)
        writer=open(fll,'a')

        for r in range(0,R):
        	r=torch.tensor([r]).cuda()
        	r_embed = F.normalize(self.rel_embed(r), 2, -1)
        	writer.writelines(str(torch.flatten(r_embed).detach().cpu().numpy()))
        	writer.writelines('\n')

        writer.close()

class TransHModule(BaseModule):
    def __init__(self, n_ent, n_rel, args):
        super(TransHModule, self).__init__(n_ent, n_rel, args)
        self.rel_embed = nn.Embedding(n_rel, args.hidden_dim)
        self.ent_embed = nn.Embedding(n_ent, args.hidden_dim)
        self.proj_rel_embed = nn.Embedding(n_rel, args.hidden_dim)
        self.init_weight()

    def forward(self, head, tail, rela):
        shape = head.size()
        head = head.contiguous().view(-1)
        tail = tail.contiguous().view(-1)
        rela = rela.contiguous().view(-1)
        head_embed = F.normalize(self.ent_embed(head), 2, -1)
        tail_embed = F.normalize(self.ent_embed(tail), 2, -1)
        rela_embed = F.normalize(self.rel_embed(rela), 2, -1)
        w_embed = F.normalize(self.proj_rel_embed(rela), 2, -1)
        head_proj = head_embed - torch.sum(w_embed * head_embed, dim=-1, keepdim=True) * w_embed
        tail_proj = tail_embed - torch.sum(w_embed * tail_embed, dim=-1, keepdim=True) * w_embed
        return torch.norm(tail_proj - head_proj - rela_embed, p=self.p, dim=-1).view(shape)

    def prob_logit(self, head, tail, rela, flag):
        return -self.candidate(head, tail, rela, flag) / self.temp

    def writeemb(self, N, R, task_dir, mdl):
        fll=os.path.join(task_dir, mdl +'_Em_Ent.txt')
        if(os.path.exists(fll)):
            os.remove(fll)
        writer=open(fll,'a')
        for e in range(0,N):
        	e=torch.tensor([e]).cuda()
        	e_embed = F.normalize(self.ent_embed(e), 2, -1)
        	writer.writelines(str(torch.flatten(e_embed).detach().cpu().numpy()))
        	writer.writelines('\n')
        writer.close()

        fll=os.path.join(task_dir, mdl +'_Em_Rel.txt')
        if(os.path.exists(fll)):
            os.remove(fll)
        writer=open(fll,'a')
        for r in range(0,R):
        	r=torch.tensor([r]).cuda()
        	r_embed = F.normalize(self.rel_embed(r), 2, -1)
        	writer.writelines(str(torch.flatten(r_embed).detach().cpu().numpy()))
        	writer.writelines('\n')
        writer.close()

        fll=os.path.join(task_dir, mdl +'_Em_Rel_M.txt')
        if(os.path.exists(fll)):
            os.remove(fll)
        writer=open(fll,'a')
        for w in range(0,R):
        	w=torch.tensor([w]).cuda()
        	w_embed = F.normalize(self.proj_rel_embed(w), 2, -1)
        	writer.writelines(str(torch.flatten(w_embed).detach().cpu().numpy()))
        	writer.writelines('\n')
        writer.close()

    def candidate(self, ent1, ent2, rela, flag):
        shape = ent1.size()
        ent1 = ent1.contiguous().view(-1)
        ent2 = ent2.contiguous().view(-1)
        rela = rela.contiguous().view(-1)
        ent1_embed = F.normalize(self.ent_embed(ent1), 2, -1)
        ent2_embed = F.normalize(self.ent_embed(ent2), 2, -1)
        rela_embed = F.normalize(self.rel_embed(rela), 2, -1)
        w_embed = F.normalize(self.proj_rel_embed(rela), 2, -1)
        ent1_proj = ent1_embed - torch.sum(w_embed * ent1_embed, dim=-1, keepdim=True) * w_embed
        ent2_proj = ent2_embed - torch.sum(w_embed * ent2_embed, dim=-1, keepdim=True) * w_embed
        vec=torch.norm(ent1_proj - ent2_proj, p=self.p, dim=-1).view(shape)
        vec=torch.abs(vec)
        return vec

    def dist(self, head, tail, rela):
        return self.forward(head, tail, rela)

    def score(self, head, tail, rela):
        return self.forward(head, tail, rela)

class DistMultModule(BaseModule):
    def __init__(self, n_ent, n_rel, args):
        super(DistMultModule, self).__init__(n_ent, n_rel, args)
        self.ent_embed = nn.Embedding(n_ent, args.hidden_dim)
        self.rel_embed = nn.Embedding(n_rel, args.hidden_dim)
        self.init_weight()

    def forward(self, head, tail, rela):
        shapes = head.size()
        head = head.contiguous().view(-1)
        tail = tail.contiguous().view(-1)
        rela = rela.contiguous().view(-1)

        head_embed = self.ent_embed(head)
        tail_embed = self.ent_embed(tail)
        rela_embed = self.rel_embed(rela)
        return torch.sum(tail_embed * head_embed * rela_embed, dim=-1).view(shapes)
    
    def candidate(self, ent1, ent2, rela, flag):
        shape = ent1.size()
        ent1 = ent1.contiguous().view(-1)
        ent2 = ent2.contiguous().view(-1)
        rela = rela.contiguous().view(-1)
        ent1_embed = F.normalize(self.ent_embed(ent1), 2, -1)
        ent2_embed = F.normalize(self.ent_embed(ent2), 2, -1)
        vec=torch.norm(ent1_embed - ent2_embed, p=self.p, dim=-1).view(shape)
        vec=torch.abs(vec)
        return vec

    def prob_logit(self, head, tail, rela, flag):
        return -self.candidate(head, tail, rela, flag) / self.temp

    def writeemb(self, N, R, task_dir, mdl):
        fll=os.path.join(task_dir, mdl +'_Em_Ent.txt')
        if(os.path.exists(fll)):
            os.remove(fll)
        writer=open(fll,'a')
        for e in range(0,N):
        	e=torch.tensor([e]).cuda()
        	e_embed = F.normalize(self.ent_embed(e), 2, -1)
        	writer.writelines(str(torch.flatten(e_embed).detach().cpu().numpy()))
        	writer.writelines('\n')
        writer.close()

        fll=os.path.join(task_dir, mdl +'_Em_Rel.txt')
        if(os.path.exists(fll)):
            os.remove(fll)
        writer=open(fll,'a')
        for r in range(0,R):
        	r=torch.tensor([r]).cuda()
        	r_embed = F.normalize(self.rel_embed(r), 2, -1)
        	writer.writelines(str(torch.flatten(r_embed).detach().cpu().numpy()))
        	writer.writelines('\n')
        writer.close()

    def dist(self, head, tail, rela):
        return -self.forward(head, tail, rela)

    def score(self, head, tail, rela):
        return -self.forward(head, tail, rela)

