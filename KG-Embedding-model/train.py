import os 
import argparse
import torch
import warnings
from corrupter import BernCorrupter
from utils import DataLoader
from base_model import BaseModel

parser = argparse.ArgumentParser(description="Parser for Knowledge Graph Embedding")
parser.add_argument('--task_dir', type=str, default='./KG_Data/DRKG_v3', help='the directory to dataset')
parser.add_argument('--model', type=str, default='TransH',  help='Embedding model [TransE, TransH, DistMult]')
parser.add_argument('--loss', type=str, default='pair', help='Training loss [pair, point]')
parser.add_argument('--margin', type=float, default=4.0, help='Margin for pairwise training')
parser.add_argument('--lamb', type=float, default=0.01, help='Decay rate for optimizer')
parser.add_argument('--hidden_dim', type=int, default=100, help='Embedding size')
parser.add_argument('--temp', type=float, default=2.0, help='Temporature value to avoid device trigger')
parser.add_argument('--gpu', type=str, default='0', help='GPU number')
parser.add_argument('--p', type=int, default=1, help='The norm for scoring function')
parser.add_argument('--lr', type=float, default=0.001, help='Learning rate for optimizer')
parser.add_argument('--n_epoch', type=int, default=10000, help='number of training epochs')
parser.add_argument('--n_batch', type=int, default=4096, help='number of batch size')
parser.add_argument('--N1', type=int, default=100, help='cache_size')
parser.add_argument('--N2', type=int, default=3, help='random subset size')
parser.add_argument('--n_sample', type=int, default=1, help='number of negative samples')
parser.add_argument('--test_batch_size', type=int, default=30, help='test batch size')

args = parser.parse_args()

if __name__ == '__main__':
    os.environ["OMP_NUM_THREADS"] = "5"
    os.environ["MKL_NUM_THREADS"] = "5"
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    torch.set_num_threads(5)
    warnings.filterwarnings("ignore", category=UserWarning)

    dataset = args.task_dir.split('/')[-1]

    task_dir = args.task_dir
    loader = DataLoader(task_dir, args.n_sample)

    n_ent, n_rel = loader.graph_size()

    train_data = loader.load_data('train')
    valid_data = loader.load_data('valid')
    test_data  = loader.load_data('test')
    args.n_train = len(train_data[0])
    print("Number of triples:: train:{}, valid:{}, test:{}.".format(len(train_data[0]), len(valid_data[0]), len(test_data[0])))

    heads, tails = loader.heads_tails()
    head_idx, tail_idx, head_LRS, tail_LRS, head_pos, tail_pos = loader.initialize_LRS()
    LRS = [head_idx, tail_idx, head_LRS, tail_LRS, head_pos, tail_pos]

    train_data = [torch.LongTensor(vec) for vec in train_data]
    valid_data = [torch.LongTensor(vec) for vec in valid_data]
    test_data  = [torch.LongTensor(vec) for vec in test_data]

    tester_val = lambda: model.test_link(valid_data, n_ent, heads, tails, 'v')
    tester_tst = lambda: model.test_link(test_data, n_ent, heads, tails, 't')

    corrupter = BernCorrupter(train_data, n_ent, n_rel)
    model = BaseModel(n_ent, n_rel, args)

    best_str = model.train(train_data, LRS, corrupter, tester_tst, tester_tst)
    print('Prediction performance scores:', best_str)




